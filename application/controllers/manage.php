<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of the manage class
 *
 * @author Martian
 */
class manage extends CI_Controller{

	/** 
     * The Constructor
     */
    public function __construct() {
        parent::__construct();
    }

    /*
	 *
	 * The default entry point
    */
    public function index(){
    	$this->articles();
    }

    /**
     *
     * Parses the Views On the template
     * @params $view{the view file to load}, $data{content to be printed in the template}
     */
    function display($data = array(),$view = ''){
        $output['title'] = 'AFP CONVERGE ';
        $output['page'] = '';
        $output['cats'] = $this->afp_model->getCategories();
        $output['blogs'] = $this->afp_model->getOutlets(); 
        if (isset($data['scripts'])) {
            $output['scripts'] = $this->load->view($data['scripts'],$data,true);
        }
        else{
            $output['scripts'] = '';
        }
        if (!empty($view) && !empty($data)) {
            # code...
            $output['page'] = $data['page'];
            $output['content'] = $this->load->view($view,$data,true);
        }
        $this->parser->parse('afp/template',$output);
    }

    /**
     *
     * Assesses the presence and form of postdata submitted
     * @params postdata
     */

    function postData(){
        $post = $this->input->post();
        if(($post == false) || in_array('', $post)) {
           return false;
        }
        return $post;
    } 

    /*
     *
	 * Managing all the outlets
     */
    public function outlets($outlet = ''){
        if ($outlet == '') {
            # code...
            $data['scripts'] = 'afp/scripts/outlets';
            $data['outlets'] = $this->afp_model->getOutlets();
            $data['categories'] =  $this->afp_model->getCategories();
            $data['count'] = $this->afp_model->countPosts();
            $data['page'] = 'Outlets';
            $this->display($data,'afp/outlets');
        }
        else{
            $this->outlet($outlet);
        }
    }

    /*
     *
     * Managing all the outlets
     */
    public function outlet($outlet){
        $data['scripts'] = 'afp/scripts/outlet';
        $data['blog'] = $this->afp_model->getOutlets($outlet);
        $data['count'] = $this->afp_model->countPosts($outlet);
        $data['cats'] = $this->afp_model->getCategories();
        $data['autocats'] = $this->afp_model->autoCats($outlet);
        $data['posts'] = $this->afp_model->getPosts('',$outlet);
        $data['page'] = $data['blog']['name'];
        $this->display($data,'afp/outlet');
    }

    /*
     *
	 * Creating a new outlet
     */
    public function newOutlet(){
    	if ($post = $this->postData()) {
    		// if ($this->form_validation->run()==FALSE) { 
      //           print_r(json_encode($this->form_validation->error_array()));
      //       }
      //       else{
            	$new = $this->afp_model->newOutlet($post);
                var_dump($new);

            // }
    	}
        else{
            $this->outlets();
        }
    }

    /*
     *
     * Managing all the articles
     */
    public function articles($category = ''){
        $data['scripts'] = 'afp/scripts/articles';
        $data['articles'] = $this->afp_model->getArticles($category);
        $data['outlets'] = $this->afp_model->_index($this->afp_model->getOutlets(),'id');
        $data['posts'] = $this->afp_model->getPosts($category,'');
        $data['page'] = 'Articles';
        $this->display($data,'afp/articles');
    }

    /*
     *
     * Posting of articles
     */
    public function post(){
        if($post = $this->postdata()){
            // var_dump($post);
            $articles = $outlets = array();
            if (!empty($post['articles'])) {
                foreach ($post['articles'] as $a) {
                    if ($a['name'] == 'articles') {
                        array_push($articles, $a['value']);
                    }
                }
                if (!empty($post['outlets'])) {
                    foreach ($post['outlets'] as $o) {
                        if ($o['name'] == 'outlets') {
                            array_push($outlets, $o['value']);
                        }
                    }
                    $this->load->library('simplerpc'); 
                    $refs = $this->afp_model->getRefs($articles);
                    $recipients = $this->afp_model->getBlogs($outlets);
                    foreach ($recipients as $r) {
                        $r['url'] = stripslashes($r['url'].'/xmlrpc.php');
                        $links = array();
                        $cat = '';
                        foreach ($refs as $f) {
                            $cat = $f['cat'];
                            $links[$f['id']] = $f['post_ref'];
                        }
                        $this->simplerpc->fetchDetails($cat, $links, $r);
                    }

                }
                else{
                    print_r(PHP_EOL.'No outlets have been submitted for posting!'.PHP_EOL);
                }
            }
            else{
                print_r(PHP_EOL.'No articles have been submitted for posting!'.PHP_EOL);
            }

        }
        else{
            print_r(PHP_EOL.'No outlets or articles have been submitted for posting!'.PHP_EOL);
        }

    }

    public function dump(){
        $this->afp_model->run();
    }
    
}
