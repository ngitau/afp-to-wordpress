<div class="alert alert-success alert-block fade in">
  <button data-dismiss="alert" class="close close-sm" type="button">
      <i class="fa fa-times"></i>
  </button>
  <h4>
      <i class="fa fa-ok-sign"></i>
      AFP activity on <?php echo($blog['name']);?>.
  </h4>
  <p><strong><?php echo(number_format($count,0,'',','))?></strong> articles have been posted on this outlet so far.</p>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="panel">
          <div class="panel-body">
              <div>
                  <h4 class="terques"><?php echo($blog['name'])?></h4>
                  <p><?php echo(anchor($blog['url'],$blog['url'], array('title' => '_blank')))?></p>
                  <p style="text-transform:capitalize"><?php echo($blog['type'])?> Brand</p>
                  <p><?php echo(anchor('#myModal', ' <i class="fa fa-cog"></i> Edit Outlet', array('data-toggle'=>'modal')))?></p>
              </div>
          </div>
      </div>
	</div>
	<?php if (!empty($autocats)) {
		?>
		<div class="col-lg-6">
			<div class="panel">
	          <div class="panel-body">
	              <div>
	                  <h4 class="terques">Auto Post Categories</h4>
	                  <p><button data-toggle="button" class="btn btn-white"><?php if ($blog['publish'] == 1): ?><i class="fa fa-save"></i> Auto Publish Enabled<?php else : ?><i class="fa fa-frown-o"></i> Auto Publish Disabled <?php endif ?></button></p>
		              	<?php $auto = array(); foreach ($autocats as $autocat) {
		              		array_push($auto, $autocat['id']);
		              		print_r(anchor('manage/articles/'.$autocat['id'], '<span class="label label-primary" style="margin: .25em; line-height:2.5em;">'.$autocat['name'].'</span>'));
		              	}?>
	              </div>
	          </div>
	      </div>
		</div>
		<?php
	}
	?>
	<!-- <div class="col-lg-4">
		<div class="panel">
          <div class="panel-body">
              <div>
                  <h4></h4>
                  <p></p>
              </div>
          </div>
      </div>
	</div> -->
</div>
<?php if (!empty($posts)) :?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
	          <header class="panel-heading">
	              Recently Posted Articles <span class="label label-default"><?php echo count($posts);?></span>
	          </header>
	          <div class="panel-body">
	              <div class="adv-table">
                    <table  class="display table table-bordered table-striped">
                      <thead>
	                      <tr>
	                      	  <th>Posted</th>
	                          <th>Article Name</th>
	                          <th></th>
	                      </tr>
                      </thead>
                      <tbody>
	                      <?php foreach ($posts as $p): ?>
	                      <tr>
	                      	  <td><?php echo(date('d.M.y, h:i a',$p['date']))?></td>	
	                          <td><?php echo($p['post_title'])?></td>
	                          <td><?php echo(anchor($blog['url'].'?p='.$p['post_id'], ' <i class="fa fa-external-link"></i> ', array('target' => '_blank')))?></td>
	                      </tr>		
	                      <?php endforeach;?>
                      </tbody>
                      <!-- <tfoot>
	                      <tr>
	                      	  <th></th>
	                          <th></th>
	                          <th></th>
	                      </tr>
                      </tfoot> -->
                    </table>                    
                </div>
	          </div>
	    </section>
	</div>
</div>
<?php else:?>
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
	          <header class="panel-heading">
	              No Recently Posted Articles To Show!
	          </header>
	    </section>
	</div>
</div>	
<?php endif; ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Edit <?php echo($blog['name'])?>'s details.</h4>
          </div>
          <form>
          	  <div class="row modal-body">
		        <form method="post">
		        	<div class="col-lg-6">
		        		<h4>Site Details</h4>
		        		<hr>
		            	<div class="form-group">
                            <label for="url">Site Url</label>
                            <input type="text" class="form-control" name="url" value="<?php echo $blog['url']?>" placeholder="Enter Url">
                        </div>
                        <div class="form-group">
                            <label for="user">Publishing User</label>
                            <input type="text" class="form-control" name="user" value="<?php echo $blog['user']?>" placeholder="Enter User">
                        </div>
                        <div class="form-group">
                            <label for="url">User Password</label>
                            <input type="pass" class="form-control" name="pass" value="<?php echo $blog['pass']?>" placeholder="Enter Password">
                        </div>
                        <div class="checkbox">
	                        <label>
	                            <?php if ($blog['publish'] == 1): ?>
									<input type="checkbox" name="publish" value="1" checked="checked"> Allow automated Posting
								<?php else:?>
									<input type="checkbox" name="publish" value="1"> Allow automated posting
	                            <?php endif ?>
	                        </label>
	                    </div>
		            </div>    
		            <div class="col-lg-6">
		            	<h4>Auto-post categories</h4>
		            	<hr>
		            	<?php foreach ($cats as $cat): ?>
		            		<div class="row" style="padding: .5em 1em;">
		            			<?php if (in_array($cat['id'], $auto)): ?>
			            			<input type="checkbox" name="category" value="<?php echo($cat['id'])?>" checked="checked"> <?php echo($cat['name'])?>
			            		<?php else: ?>
			            			<input type="checkbox" name="category" value="<?php echo($cat['id'])?>"> <?php echo($cat['name'])?>	
			            		<?php endif ?>
		            		</div>
		            	<?php endforeach ?>
		            </div>
		        </form>
	          </div>
	          <div class="modal-footer">
	              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	              <button class="btn btn-success" type="button">Save changes</button>
	          </div>	
          </form>
      </div>
  </div>
</div>
<!-- modal -->