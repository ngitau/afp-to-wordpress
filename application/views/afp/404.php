<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AFP Feed Aggregator">
    <meta name="author" content="Ndung'u Gitau">
    <meta name="keyword" content="Radio Africa, AFP Aggregator">
    <link rel="shortcut icon" href="<?php echo base_url()?>flatlab/img/favicon.png">

    <title>Ooops :: 404</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>flatlab/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>flatlab/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url()?>flatlab/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>flatlab/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>flatlab/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>




  <body class="body-404">

    <div class="container">

      <section class="error-wrapper">
          <i class="icon-404"></i>
          <h1>404</h1>
          <h2>Stay on your tuff!</h2>
          <p class="page-404">The page you are trying to load doesn't exist on the face of the earth!. <a href="<?php echo base_url()?>">Roll Back</a></p>
      </section>

    </div>


  </body>
</html>
