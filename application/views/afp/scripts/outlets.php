<script>
function listCats(panel){
	if (panel.is(':checked')) {
		$('.cats').removeClass('hide');
	} else {
		$('.cats').addClass('hide');
	}
}
$(document).ready(function(event) {
	listCats($('input[name="publish"]'));
});
$('input[name="publish"]').bind('change', function(event) {
	listCats($(this));
});
$('.new-outlet').submit(function(event) {
	/* Act on the event */
	event.preventDefault();

	
	var outlet = $(this).serializeArray();
	$.post('<?php echo base_url("manage/newoutlet") ?>', outlet, function(data, textStatus, xhr) {
		console.log(data);
	});
});
</script>