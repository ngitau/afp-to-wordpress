<link href="<?php echo base_url()?>flatlab/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="<?php echo base_url()?>flatlab/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url()?>flatlab/assets/data-tables/DT_bootstrap.css" />
<script type="text/javascript" language="javascript" src="<?php echo base_url()?>flatlab/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>flatlab/assets/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
  	var articles = '';
  	$('table.table').each(function(index, el) {
  		$(this).dataTable( {
          "aaSorting": [[ 4, "desc" ]]
        });
  	});
  });	
</script>  	