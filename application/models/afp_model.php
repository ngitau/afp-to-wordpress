<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of afp_model
 *
 * @author Joseph Ndung'u Gitau
 */
class afp_model extends CI_Model {
    
    protected $salt_length;
    private $span;
    private $period;
    function __construct() {
        parent::__construct();
        $this->salt_length = 10;
        $this->span = time() - 86400;
        $this->period = time() - 86400*3; 
        date_default_timezone_set('Africa/Nairobi');
        $this->load->library('encrypt');
    }
    
    /**
     * Insert records in the db
     *
     * @params $dataset, $table
     * 
     */
    
    function _insert($set,$table){
        $this->db->insert($table, $set);
        if($this->db->affected_rows()) {
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * Update records in the db
     *
     * @params $dataset, $table, $wherefield $wherevalue
     * 
     */
    
    function _update($set,$table,$field,$value){
        $this->db->where($field, $value);
        $this->db->update($table, $set); 
        return true;   
    }
    
    /**
     * Delete records in the db
     *
     * @params $whereset, $table
     * 
     */
    
    function _delete($set, $table){
        $this->db->delete($table, $set); 
        if($this->db->affected_rows()) {
             return true;
        }
        else{
             return false;
        }
    }

    /**
     * Index results
     *
     * @params $result, $index
     * 
     */
    
    function _index($result, $index){
       $indexed = array();
       foreach ($result as $r) {
           $indexed[$r[$index]] = $r;
       }
       unset($result);
       return $indexed;
    }

    /**
     * Add a ref in the haystack
     *
     * @params $posts
     * 
     */
    function logArticles($posts){
        foreach ($posts as $post) {
            $table = 'haystack';
            $post['date'] = time();
            $this->_insert($post, $table);
        }
    }

    /**
     * Check if a ref has been stacked
     *
     * @params $ref
     * 
     */
    function checkRef($cat,$ref){
        $this->db->where('category', $cat);
        $this->db->where('post_ref', $ref);
        return $this->db->count_all_results('haystack');

    }

    /**
     * Fetch last ref added in the db
     *
     * @params none
     * 
     */
    function lastRef($cat = 1){
        $this->db->select('post_ref')
                ->where('category', $cat)
                ->from('haystack')
                ->order_by('id','desc')
                ->limit(1);
        $result = $this->db->get()->row_array();
        if(!empty($result)){
            return $result['post_ref'];
        }
        else{
            return false;
        }
    }

    /*
     *
     * Fetch all Categories 
     */
    function getCategories(){
        return $this->db->select('id,name')->where('status',1)->get('category')->result_array();
    }

    /*
     *
     * Create a new Category 
     */
    function newCategory($cats){
        foreach ($cats as $cat) {
            $post['name'] = $cat;
            $this->_insert($post,'category');
        }
    }

    /*
     *
     * Delete Category 
     */
    function deleteCategory($cats){
        foreach ($cats as $cat) {
            var_dump($cat);
            $this->_delete(array('name'=> $cat),'category');
        }
    }

    /*
     *
     * Fetch all Outlets 
     */
    function getOutlets($id = ''){
        if ($id == '') {
            # code...
            return $this->db->get('outlet')->result_array();
        }
        else{
            return $this->db->where('id',$id)->get('outlet')->row_array();
        }
    }

    /*
     *
     * Create a new Outlet 
     */
    function newOutlet($outlet){
        $postcats = $outlet['categories'];
        unset($outlet['categories']);
        $outlet['url'] = 'http://'.$outlet['url'];
        $this->_insert($outlet,'outlet');
        $set['outlet'] = $this->db->insert_id();
        foreach ($postcats as $postcat) {
            $set['category'] = $postcat;
            $this->_insert($set,'autopost');
        }
        return true;
    }

    /*
     *
     * Fetch all Articles 
     */
    function getArticles($category){
        $this->db->where('date >', $this->period)->where('status !=', 1)->order_by('date','desc');
        if ($category == '') {
            return $this->db->get('haystack')->result_array();
        }
        else{
            return $this->db->where('category',$category)->get('haystack')->result_array();
        }
        
    }

    /*
     *
     * Fetch all Posts 
     */
    function getPosts($category,$outlet){
        $this->db->distinct()
            ->select('post.article_id, haystack.post_title, post.post_id, post.outlet_id, post.date, haystack.id')
            ->where('haystack.date >', $this->period)
            ->from('post')
            ->join('haystack','haystack.id = post.article_id')
            ->order_by('post.date','desc')
            ->group_by('post.article_id')
            ->group_by('post.outlet_id');

        if ($outlet != '') {
            return $this->db->where('post.outlet_id',$outlet)->get()->result_array();
        }
        else if ($category == '' && $outlet == '') {
            return $this->db->get()->result_array();
        }
        else{
            return $this->db->where('category',$category)->get()->result_array();
        }    
    }

    /*
     *
     * Count all Posts
     */
    function countPosts($outlet = ''){
        if ($outlet != '') {
            $this->db->where('outlet_id', $outlet);
        }
        return $this->db->count_all_results('post');
    }

    /*
     *
     * Fetch outlets' uris 
     */
    function getBlogs($outlets){
        $this->db->select('id,url,user,pass')->where_in('id',$outlets);
        return $this->db->get('outlet')->result_array();
    }

    /*
     *
     * Fetch autopost outlets
     */
    function autoOutlets(){
        $this->db->select('id,url,user,pass')->where('publish',1);
        return $this->db->get('outlet')->result_array();
    }

    /*
     *
     * Fetch autopost categories for an outlet
     */
    function autoCats($outlet){
        $this->db->select('category.id,category.name')->where('autopost.status',1)->where('autopost.outlet',$outlet)->join('category','autopost.category = category.id');
        return $this->db->get('autopost')->result_array();
    }

    /*
     *
     * Fetch articles' refs 
     */
    function getRefs($articles){
        $this->db->select('haystack.id,haystack.post_ref,category.name as cat')->where_in('haystack.id',$articles)->where('haystack.status !=', 2)->join('category','haystack.category = category.id');
        return $this->db->get('haystack')->result_array();
    }

    /*
     *
     * Fetch unposted articles
     */
    function unPosted($cats){
        $this->db->select('haystack.id,haystack.post_ref,category.name as cat,haystack.status')->where_in('haystack.category', $cats)->where('date >', $this->span)->where('haystack.status !=', 2)->join('category','haystack.category = category.id');
        return $this->db->get('haystack')->result_array();
        // return $this->db->last_query();
    }

    /*
     *
     * Fetch post categories
     */
    function postCats($outlet){
        $this->db->select('category,taxonomy')->where('outlet',$outlet)->where('status',1);
        return $this->db->get('autopost')->result_array();
    }

    /*
     *
     * Fetch set taxonomy
     */
    function getTax($category, $outlet){
        $this->db->select('autopost.taxonomy')->where('category.name',$category)->where('autopost.outlet',$outlet)->join('category','category.id = autopost.category');
        return $this->db->get('autopost')->row_array();
    }

    /**
     * Add a posting in the post
     *
     * @params $post
     * 
     */
    function checkPost($post,$blog){
        $this->db->where('article_id', $post);
        $this->db->where('outlet_id', $blog);
        $this->db->where('status', 0);
        return $this->db->count_all_results('post');

    }

    /**
     * Add a posting in the post
     *
     * @params $post
     * 
     */
    function markPost($post){
        $table = 'post';
        $post['date'] = time();
        $this->_insert($post, $table);
        $this->_update(array('status' => 1),'haystack','id', $post['article_id']);
    }

     /**
     * Note bad refs
     *
     * @params $refid
     * 
     */
    function markRef($refid, $status){
        $this->_update(array('status' => $status),'haystack','id', $refid);
    }

    /**
     * Fetch faulty ref
     *
     * @params none
     * 
     */
    function faultyRefs($category = ''){
        $this->db->select('haystack.id,category.name,haystack.post_ref')->where('haystack.status', 2)->join('category','haystack.category = category.id')->order_by('date','desc');
        if ($category == '') {
            return $this->db->get('haystack')->result_array();
        }
        else{
            return $this->db->where('category',$category)->get('haystack')->result_array();
        }
    }

    function run(){
        $this->db->select('pass');
        $passes = $this->db->get('outlet')->result_array();
        foreach ($passes as $p) {
            $pass = $this->encrypt->encode($p['pass']);
            var_dump($p['pass']); var_dump($pass); var_dump($this->encrypt->decode($pass));
        }
    }
}    
