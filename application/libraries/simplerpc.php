<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of simplerpc
 *
 * @author Martian
 */
class simplerpc extends CI_Controller{

	/** 
    * The Constructor
    */
    public function __construct() {
        parent::__construct();
        $this->load->library('simplexml');
    }

    /*
     * Fetch each posts' details from the $ref file
     * @params $refs
     * @return $posts
     */
    public function fetchDetails($cat, $refs, $blog){
        $this->xmlrpc->server($blog['url'], 80);
        print_r(PHP_EOL.'** Handling posts to the blog at '.$blog['url'].'.'.PHP_EOL);
        $path =  AFP_FEED.$cat.'/';
        foreach ($refs as $refid => $ref){
            $dup = intval($this->afp_model->checkPost($refid,$blog['id']));
            if ( $dup == 0) {
            	$ref = $path.$ref;
            	$post = $thumbnail = $attachment = array();                
                if ($details = @file_get_contents($ref)) {
                    $info = $this->simplexml->xml_parseObj($details); 
                    $revisions = $info->NewsItem->NewsManagement;
                    $post['post_title'] = stripcslashes($info->NewsItem->NewsComponent->NewsLines->HeadLine);

                    $content = $info->NewsItem->NewsComponent->NewsComponent;
                    if (is_array($content)){
                        $copy = $content[0]->ContentItem->DataContent->p;
                        unset($content[0]);
                        if(!empty($content)){
                            $thumb = $content[1];
                            $thumbnail['title'] = stripcslashes($thumb->NewsLines->HeadLine);
                            $thumbnail['src'] = $path.$thumb->NewsComponent[1]->ContentItem->attributes->Href;
                        }
                        unset($content[1]);
                        if(!empty($content)){
                           $attached = $content[2];
                           $attachment['title'] = stripcslashes($attached->NewsLines->HeadLine);
                           $attachment['src'] = $path.$attached->NewsComponent[1]->ContentItem->attributes->Href;
                        }
                        unset($content[2]);
                    }
                    else if (is_object($content)) {
                        $copy = $content->ContentItem->DataContent->p;
                    }
                    else{
                        print_r(PHP_EOL.'- Can not retrieve content for "'.$post['post_title'].'". '.$content.PHP_EOL);
                    }

                    if (isset($copy)) {
                        $post['post_content'] = '';
                        foreach ($copy as $c) {
                            if (is_string($c)) {
                                $post['post_content']  = $post['post_content'].'<p>'.stripcslashes($c).'</p>';
                            }
                        }
                        $post['post_content'] = $post['post_content'].'<p><strong>Photo Credits : AFP</strong></p>';
                        $excerpt = substr($post['post_content'], 0, 140);
                        $pos = strrpos($excerpt, ' ');
                        if ($pos > 0) {
                            $excerpt = substr($excerpt, 0, $pos);
                        }
                        $post['post_excerpt'] = $excerpt;

                        $term = $this->afp_model->getTax($cat,$blog['id']);

                        $postid = $this->newPost($post, $blog);

                        if ($postid) {
                            if (!empty($thumbnail)) {
                                $featured = $this->uploadFile($postid,$thumbnail,$blog);
                                unset($thumbnail);
                            }
                            if (!empty($attachment)) {
                                $attached = $this->uploadFile($postid,$attachment,$blog);
                                unset($attachment);

                            }
                            $mark = array('post_id' => $postid, 'article_id' => $refid, 'outlet_id' => $blog['id'],);
                            $this->afp_model->markPost($mark);
                            print_r(PHP_EOL.'- Added : '.$post['post_title'].'.'.PHP_EOL);
                            unset($postid);
                        }
                    }
                    else{
                        print_r(PHP_EOL.'- A problem occurred while retrieving content for "'.$post['post_title'].'". '.$content.PHP_EOL);
                    }
                }
                else {
                    $this->afp_model->markRef($refid,2);
                    print_r(PHP_EOL.'- The xml file with article details for '.$refid.' could not be found!'.PHP_EOL);
                }
            }
            else{
                print_r(PHP_EOL.'- Article '.$refid.' has already been posted to '.$blog['url'].'.'.PHP_EOL);
            }    
        }
        print_r(PHP_EOL);
    }

    /*
     * Make an xmlrpc call
     * @params $request
     * @return $response
     */
    public function makeCall($request){
    	// $this->xmlrpc->set_debug(TRUE);
    	$this->xmlrpc->request($request);
		if ( ! $this->xmlrpc->send_request()){
		    print_r(PHP_EOL.'- '.$this->xmlrpc->display_error());
		}
		else{
			return $this->xmlrpc->display_response();
		}
    }

    /*
     * Get all posts on wp using the xmlrpc class
     * 
     */
    public function getPosts($blog){
		$this->xmlrpc->method('wp.getPosts');
		$request = array('1',$blog['user'],$blog['pass']);
		return $this->makeCall($request);
    }

    /*
     * Get a taxonomy on wp using the xmlrpc class
     * 
     */
    public function getTaxonomy($taxonomy, $blog){
        $this->xmlrpc->method('wp.getTaxonomy');
        $request = array('1',$blog['user'],$blog['pass'],$taxonomy);
        return $this->makeCall($request);
    }

    /*
     * Create a new post on wp using the xmlrpc class
     * 
     */
    public function newPost($post, $blog){
    	$this->xmlrpc->method('wp.newPost');
    	date_default_timezone_set('UTC');
    	// print_r($blog); // die();
    	$params = array('post_type'=>'post','post_status' => 'publish','post_date_gmt' => date('Y-m-d H:i:s'));
    	if (in_array($blog['id'], array('3','4','6','7'))) {
    		$params['post_status'] = 'scheduled';
    	}
    	$content = array_merge($post,$params);
    	$request = array(
    		array('1','string'),
    		array($blog['user'],'string'),
    		array($blog['pass'],'string'),
    		array($content,'struct'),
    		array(TRUE,'boolean'),
    		);
    	return $this->makeCall($request);
    }

    /*
     * Upload a new file on wp using the IXR_Library
     * 
     */
    public function uploadFile($postid, $image, $blog){
    	include_once(APPPATH.'libraries/IXR_Library.php');
    	$client = new IXR_Client($blog['url']);
    	$title = strtolower(substr($image['title'],0,140)).'.jpg';
    	$type = get_mime_by_extension($image['src']);
    	$bits = file_get_contents($image['src']);
    	$bits = new IXR_Base64($bits);

    	$request = $client->query('wp.uploadFile',1,$blog['user'],$blog['pass'],array('name' => $title, 'type' => $type, 'bits' => $bits, 'overwrite' => TRUE, 'post_id' => $postid));
    	$response = $client->getResponse();
    	$append = array('post_thumbnail' => $response['id']);
    	$this->editPost($postid,$append,$blog);
    	//return $response;
    }

    /*
     * Get file url on wp using the IXR_Library
     * 
     */
    public function getMediaItem($itemid, $blog){
    	$this->xmlrpc->method('wp.getMediaItem');
    	$request = array(
    		array('1','string'),
    		array($blog['user'],'string'),
    		array($blog['pass'],'string'),
    		array($itemid,'int')
    		);
    	return $this->makeCall($request);
    }  

	/*
	* Edit an existing post on wp using the xmlrpc class
	* 
	*/ 
	public function editPost($postid, $struct, $blog){
		$this->xmlrpc->method('wp.editPost');
    	$request = array(
    		array('1','string'),
    		array($blog['user'],'string'),
    		array($blog['pass'],'string'),
    		array($postid,'int'),
    		array($struct,'struct'),
    		);
    	$this->makeCall($request);
	}

	/*
	* Unit testing entry point
	* 
	*/ 
	function test(){
		$item = $this->getMediaItem(346);
		$parent = $item['parent'];
		$struct = '<p><a href="'.$item['link'].'"><img class="aligncenter" src="'.$item['link'].'" alt="'.$item['title'].'"></a></p>';
		print_r($struct);
	}    
}